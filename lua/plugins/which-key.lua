return { -- Useful plugin to show you pending keybinds.
	"folke/which-key.nvim",
	event = "VeryLazy", -- Sets the loading event to 'VimEnter'
	config = function() -- This is the function that runs, AFTER loading
		vim.o.timeout = true
		vim.o.timeoutlen = 500

		require("which-key").setup()

		require("which-key").add({

			{ "<leader>c", group = "[C]ode" },
			{ "<leader>d", group = "[D]ocument" },
			{ "<leader>h", group = "Git [H]unk", { mode = "v" } },
			{ "<leader>r", group = "[R]ename" },
			{ "<leader>s", group = "[S]earch" },
			{ "<leader>t", group = "[T]oggle" },
			{ "<leader>w", group = "[W]orkspace" },
			{ "<leader>x", group = "Trouble" },
		})
	end,
}
