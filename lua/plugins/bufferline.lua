-- Bufferline for pretty tabs.buff
return {
	{
		"akinsho/bufferline.nvim",
		event = { "BufReadPre", "BufNewFile" },
		dependencies = "echasnovski/mini.bufremove",
		opts = {
			options = {
				tab_size = 21,
				indicator = { style = "underline" },
				separator_style = "thick",
				color_icons = true,
				max_name_length = 20,
				max_prefix_length = 20,
				offsets = {
					{
						filetype = "Neo-tree",
						text = "File Explorer",
						highlight = "Directory",
						text_align = "left",
						separator = true,
					},
				},
				hover = {
					enabled = true,
					delay = 200,
					reveal = { "close" },
				},
				close_command = function(bufnr)
					require("mini.bufremove").delete(bufnr, false)
				end,
				right_mouse_command = function(bufnr)
					require("mini.bufremove").delete(bufnr, false)
				end,
				diagnostics = "nvim_lsp",
			},
		},
		config = function(_, opts)
			require("bufferline").setup(opts)

			-- Buffer navigation.
			vim.keymap.set("n", "<C-t>", "<cmd>BufferLineCyclePrev<cr>", { desc = "Previous buffer" })
			vim.keymap.set("n", "<C-n>", "<cmd>BufferLineCycleNext<cr>", { desc = "Next buffer" })

			require("which-key").add({
				{ "<leader>f", group = "Bu[F]fers" },
				{ "<leader>fc", "<cmd>BufferLinePickClose<cr>", desc = "Select a buffer to close" },
				{
					"<leader>fd",
					function()
						require("mini.bufremove").delete(0, true)
					end,
					desc = "Delete current buffer",
				},
				{ "<leader>fl", "<cmd>BufferLineCloseLeft<cr>", desc = "Close buffers to the left" },
				{ "<leader>fo", "<cmd>BufferLinePick<cr>", desc = "Select a buffer to open" },
				{ "<leader>fr", "<cmd>BufferLineCloseRight<cr>", desc = "Close buffers to the right" },

				{ "<leader>p", group = "+Tabs" },
				{ "<leader>pc", "<cmd>tabclose<cr>", desc = "Close tab page" },
				{ "<leader>pn", "<cmd>tab split<cr>", desc = "New tab page" },
				{ "<leader>po", "<cmd>tabonly<cr>", desc = "Close other tab pages" },
			})
		end,
	},
}
