return {
	{
		"nvim-treesitter/nvim-treesitter",
		event = { "BufReadPre", "BufNewFile" },
		build = ":TSUpdate",
		config = function()
			local treeSConfig = require("nvim-treesitter.configs")
			treeSConfig.setup({
				ensure_installed = {
					"lua",
					"javascript",
					"html",
					"vim",
					"vimdoc",
					"markdown",
					"json",
					"typescript",
					"css",
					"markdown_inline",
					"vue",
					"dockerfile",
					"bash",
				},
				auto_install = true,
				sync_installed = false,
				highlight = { enable = true },
				indent = { enable = true },
			})
		end,
	},
}
