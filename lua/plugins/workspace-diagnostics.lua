return {
  "artemave/workspace-diagnostics.nvim",
  config = function()
    require("lspconfig").ts_ls.setup({
      on_attach = function(client, bufnr)
        require("workspace-diagnostics").populate_workspace_diagnostics(client, bufnr)
      end,
    })
  end,
}
