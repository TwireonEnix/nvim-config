return {
	"numtostr/comment.nvim",
	event = { "BufReadPre", "BufNewFile" },
	dependencies = { "JoosepAlviste/nvim-ts-context-commentstring" },
	config = function()
		local comment = require("Comment")

		local tsContext = require("ts_context_commentstring.integrations.comment_nvim")
		comment.setup({ pre_hook = tsContext.create_pre_hook() })
	end,
}
