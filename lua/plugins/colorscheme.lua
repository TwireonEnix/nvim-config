local M = {}

M.nordic = {
	"AlexvZyl/nordic.nvim",
	lazy = false,
	priority = 1000,
	name = "nordic",
	config = function()
		require("nordic").setup({
			reduced_blue = true,
			transparent_bg = true,
			override = {
				NeoCodeiumSuggestion = {
					fg = "#ffee8c",
					italic = true,
					underline = true,
					undercurl = false,
				},
			},
		})
		vim.cmd("colorscheme nordic")
	end,
}

M.everforest = {
	"neanias/everforest-nvim",
	version = false,
	lazy = false,
	name = "everforest",
	priority = 1000,
	config = function()
		require("everforest").setup({
			background = "medium",
			transparent_background_level = 1,
			sign_column_background = "none",
			ui_contrast = "low",
			dim_inactive_windows = false,
			diagnostic_text_highlight = false,
			diagnostic_virtual_text = "coloured",
			spell_foreground = false,
			show_eob = true,
			on_highlights = function(hl, palette)
				hl.NeoCodeiumSuggestion = { fg = palette.orange, bg = palette.none, bold = true }
			end,
		})

		vim.cmd("colorscheme everforest")
	end,
}

M.tokyonight = {
	"folke/tokyonight.nvim",
	lazy = false,
	priority = 1000,
	config = function()
		require("tokyonight").setup({
			style = "storm",
			transparent = true,
			terminal_colors = true,
			styles = {
				sidebars = "transparent",
				floats = "transparent",
			},
			cache = true,
			on_highlights = function(hl)
				hl.NeoCodeiumSuggestion = { fg = "#f7768e", italic = true }
			end,
		})
		vim.cmd([[colorscheme tokyonight]])
	end,
}

M.catpuccin = {
	"catppuccin/nvim",
	name = "catppuccin",
	priority = 1000,
	config = function()
		require("catppuccin").setup({
			flavour = "mocha",
			transparent_background = true,
			show_end_of_buffer = true,
			integrations = {
				cmp = true,
				gitsigns = true,
				nvimtree = true,
				telescope = true,
				treesitter = true,
				ts_rainbow2 = true,
				indent_blankline = { enabled = true, colored_indent_levels = false },
			},
			custom_highlights = function(colors)
				return {
					NeoCodeiumSuggestion = { fg = colors.green },
				}
			end,
		})
		vim.cmd([[colorscheme catppuccin]])
	end,
}

M.tokyodark = {
	"tiagovla/tokyodark.nvim",
	version = false,
	lazy = false,
	name = "tokyodark",
	priority = 1000,
	config = function()
		require("tokyodark").setup({
			transparent_background = true,
			terminal_colors = true,
			custom_highlights = {
				NeoCodeiumSuggestion = { fg = "#f7768e", italic = true },
			},
		}) -- calling setup is optional
		vim.cmd([[colorscheme tokyodark]])
	end,
}

M.onedark = {
	"navarasu/onedark.nvim",
	version = false,
	lazy = false,
	name = "onedark",
	priority = 1000,
	config = function()
		require("onedark").setup({
			style = "darker",
			transparent = true,
			term_colors = true,
			highlights = {
				NeoCodeiumSuggestion = { fg = "#f7768e", italic = true },
			},
		})
		vim.cmd([[colorscheme onedark]])
	end,
}

M.kanagawa = {
	"rebelot/kanagawa.nvim",
	version = false,
	lazy = false,
	name = "kanagawa",
	priority = 1000,
	config = function()
		require("kanagawa").setup({
			style = "darker",
			transparent = true,
			terminalColors = true,
			highlights = {
				NeoCodeiumSuggestion = { fg = "#f7768e", italic = true },
			},
			colors = {
				theme = {
					all = {
						ui = {
							bg_gutter = "none",
						},
					},
				},
			},
			overrides = function()
				return {
					NeoCodeiumSuggestion = { fg = "#ff2015" },
				}
			end,
		})
		vim.cmd([[colorscheme kanagawa]])
	end,
}

M.nightfox = {
	"EdenEast/nightfox.nvim",
	version = false,
	lazy = false,
	name = "nightfox",
	priority = 1000,
	config = function()
		require("nightfox").setup({
			options = {
				transparent = true,
				terminal_colors = true,
			},
			groups = {
				all = {
					NeoCodeiumSuggestion = { fg = "#ff4715" },
				},
			},
		})
		vim.cmd([[colorscheme terafox]])
	end,
}

M.gruvbox = {
	"sainnhe/gruvbox-material",
	lazy = false,
	priority = 1000,
	name = "gruvbox-material",
	config = function()
		vim.g.gruvbox_material_transparent_background = true
		vim.g.gruvbox_material_enable_italic = true
		vim.api.nvim_create_autocmd("ColorScheme", {
			group = vim.api.nvim_create_augroup("custom_highlights_gruvboxmaterial", {}),
			pattern = "gruvbox-material",
			callback = function()
				local config = vim.fn["gruvbox_material#get_configuration"]()
				local palette =
					vim.fn["gruvbox_material#get_palette"](config.background, config.foreground, config.colors_override)
				local set_hl = vim.fn["gruvbox_material#highlight"]

				set_hl("NeoCodeiumSuggestion", palette.yellow, palette.none)
			end,
		})
		vim.cmd.colorscheme("gruvbox-material")
	end,
}

M.eldritch = {
	"eldritch-theme/eldritch.nvim",
	lazy = false,
	priority = 1000,
	name = "eldritch",
	config = function()
		require("eldritch").setup({
			transparent = true,
			on_highlights = function(hl, colors)
				hl.NeoCodeiumSuggestion = { fg = colors.white, italic = true }
			end,
		})
		vim.cmd.colorscheme("eldritch")
	end,
}

return M.everforest
