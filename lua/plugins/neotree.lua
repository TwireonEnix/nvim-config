return {
	"nvim-neo-tree/neo-tree.nvim",
	branch = "v3.x",
	dependencies = {
		"nvim-lua/plenary.nvim",
		"nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
		"MunifTanjim/nui.nvim",
		"3rd/image.nvim", -- Optional image support in preview window: See `# Preview Mode` for more information
	},
	config = function()
		require("neo-tree").setup({
			window = {
				mappings = {
					["T"] = "expand_all_nodes",
				},
			},

			event_handlers = {
				{
					event = "neo_tree_buffer_enter",
					handler = function()
						vim.cmd([[
              setlocal relativenumber
            ]])
					end,
				},
			},

			filesystem = {
				filtered_items = {
					visible = true,
					hide_dotfiles = false,
					hide_gitignored = true,
					show_hidden_count = false,
					always_show = { ".env", ".env.prod", ".env.dev", ".env.stage" },
					hide_by_name = { ".git", "node_modules" },
					never_show = { ".git", "node_modules", ".DS_Store" },
				},
			},
		})

		vim.keymap.set("n", "<C-s>", ":Neotree filesystem reveal left<CR>")
	end,
}
