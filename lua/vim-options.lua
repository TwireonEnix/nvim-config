vim.g.mapleader = " "
vim.g.maplocalleader = " "
vim.g.have_nerd_font = true


vim.opt.mouse = "a" -- to Re size mouse windows
vim.opt.showmode = false
-- search settings
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.hidden = true

vim.opt.hlsearch = true
vim.opt.expandtab = true
vim.opt.tabstop = 2
vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2
vim.opt.autoindent = true
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.scrolloff = 10
vim.opt.backspace = "indent,eol,start"
vim.opt.clipboard = "unnamedplus"
vim.opt.cursorline = true

-- sync buffers automatically
vim.opt.autoread = true
-- disable neovim generating a swapfile and showing the error
vim.opt.swapfile = false

vim.opt.termguicolors = true
vim.opt.background = "dark"
vim.opt.signcolumn = "yes"

-- split windows
vim.opt.splitright = true
vim.opt.splitbelow = true

-- spelling
vim.opt.spelllang = "en_us"
vim.opt.spell = true
vim.opt.spellsuggest = "best,9"

vim.keymap.set("n", "<Esc>", "<cmd>nohlsearch<CR>")

-- highlight on yanking
vim.api.nvim_create_autocmd("TextYankPost", {
  desc = "Highlight when yanking (copying) text",
  group = vim.api.nvim_create_augroup("kickstart-highlight-yank", { clear = true }),
  callback = function()
    vim.highlight.on_yank()
  end,
})

-- Center cursor in the middle of the screen when moving
vim.keymap.set("n", "<PageUp>", "<C-u>zz")
vim.keymap.set("n", "<PageDown>", "<C-d>zz")
vim.keymap.set("n", "<Up>", "kzz")
vim.keymap.set("n", "<Down>", "jzz")

-- Search centers cursor in the middle of the screen
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

-- Shift text down one line in normal mode
vim.keymap.set("n", "m", "o<Esc>")

-- Avoid losing pre-yanked text while pasting above a visually selected word or section
vim.keymap.set("n", "<leader>p", '"_dP')

vim.keymap.set("n", "<leader><Right>", "<C-w><Right>")
vim.keymap.set("n", "<leader><Left>", "<C-w><Left>")
vim.keymap.set("n", "<leader><Up>", "<C-w><Up>")
vim.keymap.set("n", "<leader><Down>", "<C-w><Down>")
